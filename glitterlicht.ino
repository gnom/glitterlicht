/**
 * Control WS2812B over serial
 *
 * Commands:
 *   animation:[str] -> rainbowFade, rainbow, confetti, countLeds, cyberalarm
 *   color:[hex] -> 000000 to ffffff
 *   brightness:[int] -> 0 to 255
 * Serial (USB): Debugging
 * Serial1: Commands
 */

#include "FastLED.h"

// Amount of leds
#define NUM_LEDS 300

// Data pin
#define DATA_PIN 4

// Define the array of leds
CRGB leds[NUM_LEDS];

char command[64];
char input_data[64];
bool input_handled = true;

uint8_t rainbow_fade_hue = 0;
uint8_t rainbow_hue = 0;

void setup() {
    Serial.begin(115200);
    Serial.setTimeout(1000);
    Serial1.begin(115200);
    Serial1.setTimeout(1000);

    Serial.println("Resetting LEDs");
    LEDS.addLeds<WS2812, DATA_PIN, GRB>(leds, NUM_LEDS);
    LEDS.setBrightness(255);

    Serial.println("Setup done");
}

bool readSerial(bool ignore_space = false);

void loop() {
    if (!readSerial(/* ignore_space */ true) && input_handled) {
        return;
    }
    input_handled = true;

    Serial.print("Input: '");
    Serial.print(input_data);
    Serial.println("'");

    if (startsWith(input_data, "brightness:")) {
        Serial.println("Mode: Brightness");
        setBrightness(&input_data[11]);
    } else {
        strncpy(command, input_data, 64);
    }

    if (startsWith(command, "color:")) {
        Serial.println("Mode: Color");
        setColor(&command[6]);
    } else if (startsWith(command, "animation:")) {
        Serial.println("Mode: Animation");

        if (strstr(&command[10], "rainbowFade")) {
            Serial.println("rainbowFade");
            rainbowFade();
        } else if (strstr(&command[10], "rainbow")) {
            Serial.println("rainbow");
            rainbow();
        } else if (strstr(&command[10], "countLeds")) {
            Serial.println("countLeds");
            countLeds();
        } else if (strstr(&command[10], "confetti")) {
            Serial.println("confetti");
            confetti();
        } else if (strstr(&command[10], "cyberalarm")) {
            Serial.println("cyberalarm");
            cyberalarm();
        } else {
            Serial.println("Unknown animation");
        }
    }
}

void setBrightness(char *input) {
    long brightness = strtol(input, NULL, 10);

    Serial.print("Value: ");
    Serial.print(input);
    Serial.print("/");
    Serial.println(brightness);

    LEDS.setBrightness(brightness);
    FastLED.show();
}

void setColor(char *color) {
    long led_color = strtol(color, NULL, 16);

    Serial.print("Value: ");
    Serial.print(color);
    Serial.print("/");
    Serial.println(led_color);

    for (int i = 0; i < NUM_LEDS; i++) {
        leds[i] = led_color;
    }

    FastLED.show();
}

void rainbow() {
    while (true) {
        for (int i = 0; i < NUM_LEDS; i++) {
            leds[i] = CHSV(rainbow_hue + i, 255, 255);
        }

        rainbow_hue--;
        FastLED.show();
        delay(20);

        if (readSerial()) {
            break;
        }
    }
}

void rainbowFade() {
    while (true) {
        for (int i = 0; i < NUM_LEDS; i++) {
            leds[i] = CHSV(rainbow_fade_hue, 255, 255);
        }

        rainbow_fade_hue++;
        FastLED.show();
        delay(50);

        if (readSerial()) {
            break;
        }
    }
}

void countLeds() {
    for (int i = 1; i <= NUM_LEDS; i++) {
        if (i % 100 == 0) {
            leds[i] = 0xff0000;
        } else if (i % 10 == 0) {
            leds[i] = 0x00ff00;
        } else {
            leds[i] = 0x0000ff;
        }
    }

    FastLED.show();
}

void cyberalarm() {
    while (true) {
        for (int l = 0; l < 3; l++) {
            for (int i = 0; i < NUM_LEDS; i++) {
                leds[i] = CRGB::Blue;
            }

            FastLED.show();
            delay(50);

            for (int i = 0; i < NUM_LEDS; i++) {
                leds[i] = CRGB::Black;
            }

            FastLED.show();
            delay(50);
        }

        delay(400);
        for (int l = 0; l < 3; l++) {
            for (int i = 0; i < NUM_LEDS; i++) {
                leds[i] = CRGB::Red;
            }

            FastLED.show();
            delay(50);

            for (int i = 0; i < NUM_LEDS; i++) {
                leds[i] = CRGB::Black;
            }

            FastLED.show();
            delay(50);
        }

        delay(400);

        if (readSerial()) {
            break;
        }
    }
}

void confetti() {
    int jump = 20;
    while (true) {
        fadeToBlackBy(leds, NUM_LEDS, 30);

        for (int i = 0; i < NUM_LEDS; i += jump) {
            leds[random(i, i + jump)] = CHSV(random(0, 255), 255, 255);
        }

        FastLED.show();
        delay(80);

        if (readSerial()) {
            break;
        }
    }
}

bool startsWith(char *data, char *value) {
    return strncmp(data, value, strlen(value)) == 0;
}

bool readSerial(bool ignore_space = false) {
    if (!(Serial1.available() > 0)) {
        return false;
    }

    uint8_t i = 0;
    char raw;
    while (Serial1.available() > 0) {
        // delay required because of wtf... and Serial seems to be timing sensitive... dafuq...
        delay(1);
        raw = Serial1.read();

        if (raw == '\n' || i >= sizeof(input_data) - 1) {
            input_data[i] = '\0';
            input_handled = false;
            return true;
        }

        if (raw == '\r' || (ignore_space && (raw == '\t' || raw == ' '))) {
            continue;
        }

        input_data[i] = raw;
        i++;
    }

    return false;
}
